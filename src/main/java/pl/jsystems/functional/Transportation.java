package pl.jsystems.functional;

import java.io.Serializable;
import java.util.function.Function;

@FunctionalInterface
public interface Transportation {

    /*public final static*/ int speed = 100;

    void transport(String passenger);

    // java 8
    default int getSpeed(){
        return speed;
    }

    // java 8
    static String getDescription(){
        return "transportation vehicle";
    }

    // java 9
    private String doSomething(){
        return "something";
    }

}
