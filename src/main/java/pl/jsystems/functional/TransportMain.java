package pl.jsystems.functional;

public class TransportMain {

    public static void main(String[] args) {

        String passenger = "Jan Kowalski";

        Transportation t = p -> System.out.println("teleporting " + p);
                //new Plane();

        System.out.println(
                "about to transport pasenger " + passenger + " with speed " + t.getSpeed());

        Transportation.getDescription();

        t.transport(passenger);


    }

}
