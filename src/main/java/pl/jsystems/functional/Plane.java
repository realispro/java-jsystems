package pl.jsystems.functional;

public class Plane implements Transportation{

    @Override
    public void transport(String passenger) {
        System.out.println("passenger " + passenger + " is flying");
    }

    @Override
    public int getSpeed() {
        return 400;
    }
}
