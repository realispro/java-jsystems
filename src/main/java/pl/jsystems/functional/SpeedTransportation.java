package pl.jsystems.functional;

import java.io.Serializable;

public interface SpeedTransportation extends Transportation, Serializable {

    int getTransportSpeed();
}
