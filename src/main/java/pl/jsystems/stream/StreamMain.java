package pl.jsystems.stream;

import pl.jsystems.reflect.AnimalSupplier;
import pl.jsystems.zoo.Animal;
import pl.jsystems.zoo.AnimalListSupplier;
import pl.jsystems.zoo.Grizzli;

import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamMain {

    public static void main(String[] args) {
        System.out.println("Stream API - main");

        Supplier<List<Animal>> animalListSupplier = new AnimalListSupplier();        
        List<Animal> animals = animalListSupplier.get();

        Stream<Animal> stream =  animals.parallelStream(); // ForkJoinPool

        Supplier<Animal> animalSupplier = new AnimalSupplier();

        stream = Stream.concat(stream, Stream.of(animalSupplier.get()));

        if(stream.isParallel()){
            stream = stream.sequential();
        }

        if(!stream.isParallel()){
            stream = stream.parallel();
        }

        //long count =
        //List<Animal> animalsProcessed =
        //boolean match =
        //Optional<Animal> optionalAnimal =
        List<String> names = 
                stream
                .sorted((a1, a2) -> a1.getSize() - a2.getSize())
                .peek(System.out::println) // a->System.out.println(a)
                .filter(a -> a.getName().contains("o"))
                .map(a -> a.getName())
                //.distinct()
                //.forEach(System.out::println);
                //.count();
                .collect(Collectors.toList());
                //.anyMatch(a->a.getName().contains("x"));
                //.findFirst();

        System.out.println("names = " + names);


            
        //System.out.println("match = " + match);
        //System.out.println("count = " + count);
        //System.out.println("animalsProcessed = " + animalsProcessed);

        IntStream intStream = IntStream.iterate(2, i->i*2);
                //.generate(()->102);
        intStream
                .limit(100)
                .forEach(i-> System.out.println("i:" + i));
        
    }
}
