package pl.jsystems.reflect;

import pl.jsystems.zoo.Animal;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Supplier;

public class AnimalSupplier implements Supplier<Animal> {

    private String animalClassName = "pl.jsystems.zoo.Shark";

    private String animalName = "Joe";

    private int animalSize = 444;



    private void init2(String fileName){
        Properties props = new Properties();
        try {
            props.load(ClassLoader.getSystemResourceAsStream(fileName));
            animalClassName = props.getProperty("animal.class");
            animalName = props.getProperty("animal.name");
            animalSize = Integer.parseInt(props.getProperty("animal.size"));
        } catch (IOException e) {
            throw new RuntimeException("error while configuring animal supplier", e);
        }
    }

    @Init(fileName = "animal.config")
    private void init(String fileName){
        try {
            Files
                    .lines(
                     //       Paths.get("src","main", "resources", "animal.config")
                            Paths.get(ClassLoader.getSystemResource(fileName).toURI())
                    )
                    .forEach(line->{
                        if(line.startsWith("animal.class=")){
                            animalClassName = line.substring("animal.class=".length());
                        }
                        if(line.startsWith("animal.name=")){
                            animalName = line.substring("animal.name=".length());
                        }
                        if(line.startsWith("animal.size=")){
                            animalSize = Integer.parseInt(
                                    line.substring("animal.size=".length()));
                        }
                    });
        } catch (IOException e) {
            throw new RuntimeException("animal supplier initialization error", e);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }


    @Override
    public Animal get() {

        // TODO
        Optional<Method> optionalMethod =
            Arrays.stream(this.getClass().getDeclaredMethods())
                .filter(m->m.isAnnotationPresent(Init.class))
                .findFirst();
        optionalMethod.ifPresent( m-> {
            try {
                Init init = m.getAnnotation(Init.class);
                m.invoke(this, init.fileName());
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        });

        try {
            Class animalClass = Class.forName(animalClassName);

            // TODO display if dangerous and its level

            Dangerous dangerous = (Dangerous)animalClass.getAnnotation(Dangerous.class);
            if(dangerous!=null) {
                int level = dangerous.value();
                System.out.println("Warning! dangerous animal. priority: " + level);
            }
            Constructor constructor = animalClass.getConstructor(String.class, int.class);
            Object o = constructor.newInstance(animalName, animalSize);

            // mock
            ((Animal)o).setName(null);

            //Arrays.stream(o.getClass().getFields())
            getAllFields(o.getClass()).stream()
                    .peek(System.out::println)
                    .filter(f->f.isAnnotationPresent(Autoconfigurable.class))
                    .filter(f->{
                        try {
                            f.setAccessible(true);
                            return f.get(o)==null;
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                        return false;
                    })
                    .forEach(f->{
                        Autoconfigurable autoconfigurable = f.getAnnotation(Autoconfigurable.class);
                        try {
                            f.set(o, autoconfigurable.value());
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    });

            if(o instanceof Animal){
                return (Animal) o;
            }

            throw new ClassCastException("created object not an animal");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    private List<Field> getAllFields(Class clazz){
        List<Field> fields = new ArrayList<>();
        do {
            fields.addAll(Arrays.asList(clazz.getDeclaredFields()));
            clazz = clazz.getSuperclass();
        }while(clazz!=null);
        return fields;
    }
}
