package pl.jsystems.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class ReflectMain {

    public static void main(String[] args) {
        System.out.println("ReflectMain.main");

        String className = args[0];

        try {

            Class clazz = Class.forName(className);

            do {
                printClassInfo(clazz);
                clazz = clazz.getSuperclass();
            }while(clazz!=null);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        System.out.println("done.");
    }

    public static void printClassInfo(Class clazz){
        System.out.println("******** " + clazz.getPackage().getName() + "." + clazz.getSimpleName() + " ***********");
        System.out.println("class modifiers: " + getModifiers(clazz.getModifiers()));
        Class[] interfaces = clazz.getInterfaces();
        Arrays.stream(interfaces).forEach(c->System.out.println("interface:" + c.getName()));

        Constructor[] constructors = clazz.getConstructors();
        Arrays.stream(constructors)
                .forEach(
                        c-> System.out.println("constructor: " + c.toString() + ", modifiers: " + getModifiers(c.getModifiers()))
                );

        Field[] fields = clazz.getDeclaredFields();
        Arrays.stream(fields).forEach(
                f-> System.out.println("field:" + f.toString() + ", modifiers: " + getModifiers(f.getModifiers()))
        );

        Method[] methods = clazz.getDeclaredMethods();
        Arrays.stream(methods).forEach(
                m-> System.out.println("method: " + m.toString() + ", modifiers: " + getModifiers(m.getModifiers()))
        );
    }

    private static String getModifiers(int modifiers){

        StringBuilder builder = new StringBuilder();
        builder.append("final: " + Modifier.isFinal(modifiers) + ", ");
        builder.append("abstract: " + Modifier.isAbstract(modifiers) + ", ");
        builder.append("public: " + Modifier.isPublic(modifiers) + ", ");
        builder.append("protected: " + Modifier.isProtected(modifiers) + ", ");
        builder.append("private: " + Modifier.isPrivate(modifiers) + ",");
        builder.append("transient: " + Modifier.isTransient(modifiers) + ", ");

        return builder.toString();
    }

}
