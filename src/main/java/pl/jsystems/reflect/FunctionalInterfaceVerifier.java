package pl.jsystems.reflect;

import pl.jsystems.functional.Transportation;
import pl.jsystems.zoo.Tiger;

import java.lang.reflect.Modifier;
import java.util.Arrays;

public class FunctionalInterfaceVerifier {

    public static void main(String[] args) {

        try {
            Object o = new Tiger("Tyson", 200);
            String className = args[0];
            Class clazz = o.getClass();
                    //Class.forName(className);

            if(!Modifier.isInterface(clazz.getModifiers())){
                System.out.println("Provided class is not an interface: " + className);
                return;
            }

            long count = Arrays.stream(clazz.getMethods()).filter(x ->
                    Modifier.isAbstract(x.getModifiers())
            ).count();
            if (count>1) {
                System.out.println("NIE JEST TO INTERFEJS FUNKCYJNY ");
            } else {
                System.out.println("JEST TO INTERFEJS FUNKCYJNY");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
