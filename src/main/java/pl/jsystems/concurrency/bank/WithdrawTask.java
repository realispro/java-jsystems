package pl.jsystems.concurrency.bank;

public class WithdrawTask implements Runnable{

    private Account account;

    private long start;

    public WithdrawTask(Account account, long start) {
        this.account = account;
        this.start = start;
    }

    @Override
    public void run() {
        for(int i=0; i<1_000_000; i++){
            account.withdraw(1);
        }
        long duration = System.currentTimeMillis() - start;
        System.out.println( "[" + duration+ "] Withdraw task finished. account balance: " + account.getAmount());

    }
}
