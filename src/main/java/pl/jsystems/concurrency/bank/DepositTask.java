package pl.jsystems.concurrency.bank;

public class DepositTask implements Runnable{

    private Account account;

    private long start;

    public DepositTask(Account account, long start) {
        this.account = account;
        this.start = start;
    }

    @Override
    public void run() {
        for(int i=0; i<1_000_000; i++){
            account.deposit(1);
        }
        long duration = System.currentTimeMillis() - start;
        System.out.println( "[" + duration+ "] Deposit task finished. account balance: " + account.getAmount());

    }
}
