package pl.jsystems.concurrency.bank;


import pl.jsystems.concurrency.ThreadNamePrefixPrintStream;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AccountMain {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));
        ExecutorService es = Executors.newFixedThreadPool(2);

        System.out.println("AccountMain.main");

        Account a = new Account(1_000_000);

        long start = System.currentTimeMillis();
        es.execute(new DepositTask(a, start));
        es.execute(new WithdrawTask(a, start));

        es.shutdown();

        System.out.println("done.");
    }
}
