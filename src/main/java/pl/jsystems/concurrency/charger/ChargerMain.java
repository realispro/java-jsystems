package pl.jsystems.concurrency.charger;

import pl.jsystems.concurrency.ThreadNamePrefixPrintStream;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChargerMain {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));

        Electricity.INSTANCE.turnOff();

        ExecutorService es = Executors.newFixedThreadPool(2);
        System.out.println("ChargerMain.main");

        Phone samsung = new Phone(50, "Samsung S11");
        Phone apple = new Phone(35, "Iphone XR");

        es.execute(()->samsung.charge(20));
        es.execute(()->apple.charge(30));

        es.shutdown();

        try {
            Thread.sleep(10_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Electricity.getInstance().turnOn();

        System.out.println("done.");


    }
}
