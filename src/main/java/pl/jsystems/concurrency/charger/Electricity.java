package pl.jsystems.concurrency.charger;

public enum Electricity {

    INSTANCE;

    private boolean on = false;

    private Electricity(){}

    public static Electricity getInstance(){
        return INSTANCE;
    }

    public synchronized void turnOn(){
            on = true;
            this.notifyAll();
    }

    public void turnOff(){
        on = false;
    }

    public boolean isOn() {
        return on;
    }
}
