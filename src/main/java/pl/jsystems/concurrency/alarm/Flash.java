package pl.jsystems.concurrency.alarm;

public class Flash implements Alarm {

    @Override
    public int alarm() {
        int i;
        for(i=0; i<20; i++){
            System.out.println("FLASH");
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return i;
    }

}
