package pl.jsystems.concurrency.alarm;

import pl.jsystems.concurrency.ThreadNamePrefixPrintStream;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class AlarmMain {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));

        ScheduledExecutorService es = Executors.newScheduledThreadPool(2);
                //.newFixedThreadPool(1);

        // Quartz

        System.out.println("AlarmStarter.main");

        List<Alarm> alarms = new ArrayList<>();
        alarms.add(new Beeper());
        alarms.add(new Flash());

        List<Future<Integer>> futures = new ArrayList<>();
        alarms.forEach(a-> {
            //new Thread(a).start();
            Future<Integer> future = es.submit(a);
            futures.add(future);
        });
        
        Future<Integer> future = futures.get(0);
        try {
            while(!future.isDone()){
                System.out.println("waiting....");
                Thread.sleep(3000);
            }
            int count = future.get();
            System.out.println("count = " + count);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        es.shutdown();
        System.out.println("done.");
    }
}
