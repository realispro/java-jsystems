package pl.jsystems.concurrency.alarm;


import java.util.concurrent.Callable;

public interface Alarm extends Callable<Integer> {

      int alarm();

      default void run(){
            alarm();
      }

      @Override
      default Integer call() throws Exception {
            return alarm();
      }
}
