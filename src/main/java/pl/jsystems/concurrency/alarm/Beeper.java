package pl.jsystems.concurrency.alarm;

public class Beeper implements Alarm {

    @Override
    public int alarm() {
        int i;
        for(i=0; i<30; i++){
            System.out.println("BEEP");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return i;
    }

}
