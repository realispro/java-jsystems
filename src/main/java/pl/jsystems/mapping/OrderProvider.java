package pl.jsystems.mapping;

import java.util.function.Supplier;

public interface OrderProvider extends Supplier<Order> {
}
