package pl.jsystems.mapping;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;

// JAXB = Java Architecture for XML Binding
public class JAXBOrderProvider implements OrderProvider{

    private Order order;

    public JAXBOrderProvider(InputStream stream){
        try {
            JAXBContext context = JAXBContext.newInstance(Order.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            order = (Order)unmarshaller.unmarshal(stream);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Order get() {
        return order;
    }
}
