package pl.jsystems.mapping;

import java.util.List;

public class MappingMain {

    public static void main(String[] args) {
        System.out.println("MappingMain.main");

        OrderProvider provider =
                new JSONOrderProvider(ClassLoader.getSystemResourceAsStream("order.json"));
                //new DOMOrderProvider(ClassLoader.getSystemResourceAsStream("order.xml"));
                //new JAXBOrderProvider(ClassLoader.getSystemResourceAsStream("order.xml"));
        Order order = provider.get();

        System.out.println("order received: " + order);

    }
}
