package pl.jsystems.mapping;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class JSONOrderProvider implements OrderProvider{

    private Order order;

    public JSONOrderProvider(InputStream stream){

        ObjectMapper mapper = new ObjectMapper();
        try {
            order = mapper.readValue(
                    stream,
                    Order.class);
            mapper.writeValue(new File("order.json.bak"), order);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Order get() {
        return order;
    }
}
