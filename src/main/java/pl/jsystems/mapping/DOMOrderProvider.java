package pl.jsystems.mapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class DOMOrderProvider implements OrderProvider{

    private Document document;

    public DOMOrderProvider(InputStream io){
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            document = db.parse(io);
            String date = document.getDocumentElement().getAttribute("date");
            System.out.println("parsing order at " + date);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Order get() {
        Order order = new Order();
        order.setDate(document.getDocumentElement().getAttribute("date"));
        List<Book> booksList = new ArrayList<>();

        Node booksNode = document.getDocumentElement().getElementsByTagName("books").item(0);

        if(booksNode.getNodeType() == Node.ELEMENT_NODE){
            Element booksElement = (Element) booksNode;
            NodeList booksNodeList = booksElement.getChildNodes();
            for(int i=0; i<booksNodeList.getLength(); i++){
                if(booksNodeList.item(i).getNodeType()==Node.ELEMENT_NODE) {
                    Element bookElement = (Element) booksNodeList.item(i);
                    booksList.add(parseBook(bookElement));
                }
            }
        }

        Books books = new Books();
        books.setBooks(booksList);
        order.setBooks(books);
        return order;
    }

    private Book parseBook(Element bookElement){
        Book book = new Book();

        for(int i=0; i<bookElement.getChildNodes().getLength(); i++){
            if(bookElement.getChildNodes().item(i).getNodeType()==Node.ELEMENT_NODE) {
                Element bookProperty = (Element) bookElement.getChildNodes().item(i);
                switch (bookProperty.getTagName()) {
                    case "title":
                        book.setTitle(bookProperty.getTextContent());
                        break;
                    case "author":
                        book.setAuthor(bookProperty.getTextContent());
                        break;
                    case "price":
                        book.setPrice(
                                Integer.parseInt(bookProperty.getTextContent()));
                        break;
                }
            }
        }

        return book;
    }
}
