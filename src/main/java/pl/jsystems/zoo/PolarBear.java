package pl.jsystems.zoo;

import java.io.Serializable;

public final class PolarBear extends Bear implements Serializable {

    public PolarBear(String name, int size) throws IllegalStateException{
        super(name, size);
    }

}
