package pl.jsystems.zoo;

import java.util.Comparator;

public class AnimalComparator implements Comparator<Animal> {

    private String criterion;

    private boolean ascending;

    public AnimalComparator(String criterion, boolean ascending) {
        this.criterion = criterion;
        this.ascending = ascending;
    }

    @Override
    public int compare(Animal a1, Animal a2) {

        switch (criterion){
            case "SIZE":
                return ascending
                        ? a1.getSize()-a2.getSize()
                        : a2.getSize()-a1.getSize();
            default:
                return ascending
                        ? a1.getName().compareToIgnoreCase(a2.getName())
                        : a2.getName().compareToIgnoreCase(a1.getName());
        }
    }
}
