package pl.jsystems.zoo;

import pl.jsystems.reflect.Dangerous;

@Dangerous(2)
public class Shark extends Animal {


    public Shark(String name, int size) {
        super(name, size);
    }

    @Override
    public void move() {
        System.out.println("shark is swimming like a rocket!");
    }
}
