package pl.jsystems.zoo;

import pl.jsystems.reflect.Dangerous;

@Dangerous
public abstract class Bear extends Animal {

    public Bear(String name, int size) {
        super(name, size);
    }

    @Override
    public void move() {
        System.out.println("Bear is moving very slow");
    }
}
