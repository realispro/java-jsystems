package pl.jsystems.zoo;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class ZooMain {

    public static void main(String[] args) {
        System.out.println("ZooMain.main");

        // Java Collection Framework
        Collection objects = new ArrayList();
        objects.add(new PolarBear("Jurij", 900));
        objects.add(new Shark("Stefan", 300));
        objects.add(new Tiger("Golota", 200));
        objects.add(new Grizzli("George", 800));
        objects.add(new Shark("Stefan", 300));
        objects.add(new Tiger("Tyson", 550));
        objects.add(new String("Reksio"));

        objects.add(getAnimalSupplier().get());


        // filtering
        List<Animal> animals = new ArrayList<>();
        for( Object o : objects){
            if(o instanceof Animal){
                animals.add((Animal)o);
            }
        }

        animals.removeIf(a -> a.getSize()>800); // Predicate
        animals.sort((a1, a2) -> a1.getName().compareToIgnoreCase(a2.getName()));
        //animals.sort(Comparator.comparingInt(Animal::getSize).reversed());

        Consumer<Animal> consumer = System.out::println;
                //a -> System.out.println(a);
        animals.forEach(consumer);


    }

    public static Supplier<Animal> getAnimalSupplier(){

        return () -> new Grizzli("Wojtek", 700);
    }



}
