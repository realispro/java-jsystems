package pl.jsystems.zoo;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;

public class AnimalListSupplier implements Supplier<List<Animal>> {
    @Override
    public List<Animal> get() {

        List<Animal> animals = new LinkedList<>();
        animals.add(new PolarBear("Jurij", 900));
        animals.add(new Shark("Stefan", 300));
        animals.add(new Tiger("Golota", 200));
        animals.add(new Grizzli("George", 800));
        animals.add(new Shark("Stefan", 300));
        animals.add(new Tiger("Tysonx", 550));

        return animals;
    }
}
