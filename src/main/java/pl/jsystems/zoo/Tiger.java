package pl.jsystems.zoo;

public class Tiger extends Animal{

    public Tiger(String name, int size) {
        super(name, size);
    }

    @Override
    public void move() {
        System.out.println("Tiger is running very fast");
    }
}
