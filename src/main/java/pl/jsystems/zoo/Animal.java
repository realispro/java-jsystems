package pl.jsystems.zoo;

import pl.jsystems.reflect.Autoconfigurable;

import java.util.Objects;

public abstract class Animal /*implements Comparable<Animal>*/{

    @Autoconfigurable("Unknown")
    private String name;

    private transient int size;

    public Animal(String name, int size) {
        this.name = name;
        this.size = size;
    }

    public void eat(String food){
        System.out.println(this.getClass().getSimpleName() + " is consuming " + food);
    }

    public abstract void move();

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*    @Override
    public int compareTo(Animal a) {
        // >=0 : this, a
        // <0 : a, this
        System.out.println("compare " + this + " with " + a);
        return a.size - this.size;
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return size == animal.size &&
                name.equals(animal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, size);
    }

    @Override
    public String toString() {
        return  this.getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", size=" + size +
                '}';
    }
}
