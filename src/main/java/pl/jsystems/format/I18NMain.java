package pl.jsystems.format;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

public class I18NMain {

    public static void main(String[] args) {
        // INTERNATIONALIZATION - I18N
        // LOCALIZATION - L10N

        Locale.setDefault(new Locale("en", "GB", "UNIX"));

        System.out.println(
                "lang: " + Locale.getDefault().getLanguage()
                + ", COUNTRY: " + Locale.getDefault().getCountry()
                + ", VARIANT: " + Locale.getDefault().getVariant()
        );
        ResourceBundle bundle = ResourceBundle.getBundle("messages");
        // messages_en_GB_UNIX
        // messages_en_GB
        // messages_en
        // messages

        
        String value = bundle.getString("message.key");
        System.out.println("value = " + value);

        BiMap<String, String> map = HashBiMap.create();
                //new HashMap<>();
        bundle.keySet().forEach(k->map.put(k, bundle.getString(k)));

        System.out.println("message.key: " + map.get("message.key"));
        Map<String, String> map2 = map.inverse();
        System.out.println("Sample message value: " + map2.get("Sample message value"));

        System.out.println("I18NMain.main");
        
    }
}
