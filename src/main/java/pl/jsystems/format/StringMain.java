package pl.jsystems.format;

import java.util.Objects;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringMain {

    public static void main(String[] args) {

        String s1 = "Hey!";
        String s2 = //String.valueOf("Hey!");
                new String("HEY!");
        String s3 = "Hey!";

        System.out.println(s1.equalsIgnoreCase(s2));
        System.out.println(s2.equals(s3));
        System.out.println(Objects.equals(s1, s3));
        
        String s4 = "a,b,c,de,fg";
        System.out.println("-- String splitted:");
        String[] splitted = s4.split(",");
        for(String s : splitted){
            System.out.println("s = " + s);
            if(s.equals("c")){
                System.out.println("found!");
                break;
            }
        }

        System.out.println("-- String tokens:");
        StringTokenizer tokenizer = new StringTokenizer(s4, ",");
        while (tokenizer.hasMoreTokens()){
            String s = tokenizer.nextToken();
            System.out.println("s = " + s);
            if(s.equals("c")){
                System.out.println("found!");
                break;
            }
        }

        System.out.println("-- int with scanner");
        Scanner scanner = new Scanner("1,2,3,4,5");
        scanner.useDelimiter(",");
        while(scanner.hasNext()){
            int i = scanner.nextInt();
            System.out.println("i = " + i);
            if(i==3){
                System.out.println("found!");
                break;
            }
        }

        Scanner keyboard = new Scanner(System.in);
        System.out.println("Lunch?");
        String answer = keyboard.nextLine();
        System.out.println("answer = " + answer);


        String addresses = "Warszawa 00-950, ul. Woronicza 17: Poznan 12234, ul. Sw Marcina 1/1";
        String postalCode = null; // TODO

        String patternText = "\\d\\d-?\\d\\d\\d"; // \d\d-?\d\d\d

        Pattern pattern = Pattern.compile(patternText);
        Matcher matcher = pattern.matcher(addresses);
        while(matcher.find()){
            postalCode = matcher.group();
            System.out.println("postalCode = " + postalCode);
            System.out.println("found at: " + matcher.start() + "-" + matcher.end());
        }

        String format = "[%1$5d]\n[%2$5d]";
        //System.out.printf(format, 12, 123);

        String formatted = String.format(format, 1, 23);
        System.out.println("formatted = " + formatted);


    }
}
