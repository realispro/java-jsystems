package pl.jsystems.format;

import java.text.NumberFormat;
import java.util.Locale;

public class NumberMain {

    public static void main(String[] args) {
        
        double value = 12_312.3456789;

        Locale locale = new Locale("pl", "US");
        NumberFormat format = NumberFormat.getCurrencyInstance(locale);
        format.setMinimumFractionDigits(2);
        format.setMaximumFractionDigits(5);
        format.setMinimumIntegerDigits(10);

        System.out.println("value = " + format.format(value));
        
    }
}
