package pl.jsystems.format;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateMain {

    public static void main(String[] args) {

        // date api java<8
        Date date = new Date(); // EPOCH: 1.1.1970 GMT

        Calendar calendar = Calendar.getInstance();
        //calendar.setTime(date);
        calendar.set(2020, 11, 9, 12, 19);
        calendar.add(Calendar.DATE, 7);
        date = calendar.getTime();

        Locale locale = new Locale("en", "US");
        //Locale.setDefault(locale);

        DateFormat format = new SimpleDateFormat("yyyy#MM#dd w D hh#mm##ss a G", locale);
                //DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL);
        String timestamp = format.format(date);

        System.out.println("timestamp = [" + timestamp + "]");


        // java>=8 - java.time
        LocalTime lt = LocalTime.now();
        LocalDate ld = LocalDate.of(2020, Month.DECEMBER, 9).plusDays(7);
        LocalDateTime ldt = LocalDateTime.now();

        ZonedDateTime warsawTime = ldt.atZone(ZoneId.of("Europe/Warsaw"));

        ZonedDateTime singaporeTime = warsawTime
                .withZoneSameInstant(ZoneId.of("Asia/Singapore"))
                .plusHours(7);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy~MM~dd w D hh~mm~ss a X z");

        timestamp = formatter.format(singaporeTime);
        System.out.println("java.time = [" + timestamp + "]");
        
        Duration duration = Duration.between(warsawTime, singaporeTime);
        System.out.println("duration.toHours() = " + duration.toHours());

    }
}
