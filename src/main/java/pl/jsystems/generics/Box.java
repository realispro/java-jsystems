package pl.jsystems.generics;

public class Box<T> {

    private T content;

    public void load(T o){
        this.content = o;
    }

    public T unload(){
        return this.content;
    }

}
