package pl.jsystems.generics;

import pl.jsystems.zoo.Animal;
import pl.jsystems.zoo.Bear;
import pl.jsystems.zoo.Grizzli;
import pl.jsystems.zoo.PolarBear;

import java.util.ArrayList;
import java.util.List;

public class GenericsMain {

    public static void main(String[] args) {
        System.out.println("GenericsMain.main");

        Box<Animal> animalBox = new Cage();
        animalBox.load(new PolarBear("Jurij", 900));

        Animal animal = animalBox.unload();

        List<PolarBear> bears = new ArrayList<>();
        bears.add(new PolarBear("Jurij", 900));
        //bears.add(new Grizzli("George", 800));

        // PECS - Producer extends, Consumer super
        addBear(bears);
        System.out.println("bears weight: " + sumWeight(bears));

    }

    // extends - reading
    public static int sumWeight(List<? extends Bear> bears){
        int weight = 0;
        for(Bear b : bears){
            weight+=b.getSize();
        }
        return weight;
    }

    // super - writing
    public static void addBear(List<? super PolarBear> bears){
        bears.add(new PolarBear("Sasza", 20));
        //bears.add(new Grizzli("Koralgol", 30));
    }




}
